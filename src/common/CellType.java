package common;

public enum CellType {
    EMPTY (0),
    HEAD (1),
    TAIL (2),
    CONDUCTOR (3);
    
    private final int value;
    
    CellType(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "" + value;
    }
    
    public static CellType getCellType(int value) {
        switch(value) {
            case 0:
                return EMPTY;
            case 1:
                return HEAD;
            case 2:
                return TAIL;
            case 3:
                return CONDUCTOR;
            default:
                throw new IllegalArgumentException("Invalid argument.");
        }
        
    }
}
