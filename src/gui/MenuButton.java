package gui;

import javax.swing.JButton;

public class MenuButton extends JButton {
    private boolean active;
    
    public MenuButton() {
        super();
        active = true;
    }
    
    public MenuButton(String text, boolean active) {
        super(text);
        this.active = active;
    }
}
