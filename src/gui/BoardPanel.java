package gui;

import elements.GateOR;
import elements.Element;
import elements.DiodeReversed;
import elements.Clock3Cycle;
import elements.GateXOR;
import elements.Diode;
import elements.Clock4Cycle;
import gui.listeners.BoardPanelListener;
import common.CellType;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.HashMap;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;
import logic.Board;
import logic.Generator;

public class BoardPanel extends JPanel {
    private Board board;
    private final HashMap<CellType, Color> colors;
    private final int cellSize;
    private Element selectedElement;
    private int elementX;
    private int elementY;
    
    public BoardPanel(Generator generator) {
        this.board = generator.getBoard();
        this.colors = new HashMap(4);
        this.colors.put(CellType.EMPTY, Color.black);
        this.colors.put(CellType.HEAD, Color.blue);
        this.colors.put(CellType.TAIL, Color.red);
        this.colors.put(CellType.CONDUCTOR, Color.yellow);
        this.selectedElement = null;
        MouseInputAdapter listener = new BoardPanelListener(this);
        addMouseListener(listener);
        addMouseMotionListener(listener);
        cellSize = 20;
    }
    
    public void setCell(int x, int y, CellType cellType) {
        board.setCell(x, y, cellType);
    }
    
    public CellType getCell(int x, int y) {
        return board.getCell(x, y);
    }

    public void setElementX(int elementX) {
        this.elementX = elementX;
    }

    public void setElementY(int elementY) {
        this.elementY = elementY;
    }      
    
    @Override
    public Dimension getPreferredSize()
    {
        return (new Dimension(this.board.getWidth() * cellSize, this.board.getHeight() * cellSize));
    }
    
    public void update(Generator generator) {
        this.board = generator.getBoard();
    }
    
    public void setSelectedElement(String elementName) {
        switch(elementName) {
            case "Diode": 
                selectedElement = new Diode();
                break;
            case "Reversed Diode":
                selectedElement = new DiodeReversed();
                break;
            case "OR Gate":
                selectedElement = new GateOR();
                break;
            case "XOR Gate":
                selectedElement = new GateXOR();
                break;
            case "3 Cycle Clock": 
                selectedElement = new Clock3Cycle();
                break;
            case "4 Cycle Clock": 
                selectedElement = new Clock4Cycle();
                break;
            default:
                selectedElement = null;
                break;
        }
    }
    
    public Element getSelectedElement() {
        return selectedElement;
    }
    
    public Board getBoard() {
        return board;
    }
    
    public int getCellSize() {
        return cellSize;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);        
        
        for (int i = 0; i < board.getHeight(); i++)
            for (int j = 0; j < board.getWidth(); j++) {
                g.setColor(colors.get(board.getCell(j, i)));
                g.fillRect(cellSize * j, cellSize * i, cellSize, cellSize);
            }
        g.setColor(Color.GRAY);
        for (int i = 0; i <= board.getHeight(); i++)
            g.drawLine(0, cellSize * i, board.getWidth() * cellSize, cellSize * i);
        
        for (int i = 0; i <= board.getWidth(); i++)
            g.drawLine(cellSize * i, 0, cellSize * i, board.getHeight() * cellSize); 
        
        if (selectedElement != null)
            paintGhost(g, selectedElement, elementX, elementY);
    }
    
    
    public void paintGhost(Graphics g, Element element, int x, int y) {
        if (element != null && board.isInBounds(x, y)) {
            if ((x + element.getWidth()) > board.getWidth() || (y + element.getHeight()) > board.getHeight() )
                return;
            
            for (int i = 0; i < element.getHeight(); i++)
                for (int j = 0; j < element.getWidth(); j++)
                    if (element.getCell(j, i) == CellType.CONDUCTOR) {
                        g.setColor(new Color(255, 250, 205));
                        g.fillRect((x + j) * cellSize, (y + i) * cellSize, cellSize, cellSize);
                    } else if (element.getCell(j, i) == CellType.EMPTY) {
                        g.setColor(Color.black);
                        g.fillRect((x + j) * cellSize, (y + i) * cellSize, cellSize, cellSize);
                    }
            g.setColor(Color.GRAY);
            for (int i = 0; i <= element.getHeight(); i++)
                g.drawLine(x * cellSize, (y + i) * cellSize, (element.getWidth() + x) * cellSize, (y + i) * cellSize);
        
            for (int i = 0; i <= element.getWidth(); i++)
                g.drawLine((x + i) * cellSize, y * cellSize, (x + i) * cellSize, (element.getHeight() + y) * cellSize);  
            
        }   
    }
}
