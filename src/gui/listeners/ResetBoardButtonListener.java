package gui.listeners;

import common.CellType;
import gui.BoardPanel;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import logic.Generator;

public class ResetBoardButtonListener implements ActionListener {
    private Generator generator;
    private BoardPanel boardPanel;
    
    public ResetBoardButtonListener(Generator generator, BoardPanel boardPanel) {
        this.generator = generator;
        this.boardPanel = boardPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        Window window = SwingUtilities.getWindowAncestor(button);
        
        int result = JOptionPane.showConfirmDialog(window, "Are you sure you want to reset the board?", "Trying to reset", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                for (int i = 0; i < generator.getBoard().getHeight(); i++)
                    for (int j = 0; j < generator.getBoard().getWidth(); j++)
                        generator.getBoard().setCell(j, i, CellType.EMPTY);
        
                boardPanel.update(generator);
                boardPanel.repaint();
            }
        
        
    }
    
    
}
