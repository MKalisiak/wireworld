package gui.listeners;

import gui.BoardPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import logic.Generator;

public class SingleStepButtonListener implements ActionListener{
    private Generator generator;
    private BoardPanel boardPanel;
    
    public SingleStepButtonListener(Generator generator, BoardPanel boardPanel) {
        this.generator = generator;
        this.boardPanel = boardPanel;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        generator.nextGeneration();
        boardPanel.repaint();
    }
     
}
