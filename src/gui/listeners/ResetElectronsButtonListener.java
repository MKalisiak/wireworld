package gui.listeners;

import common.CellType;
import gui.BoardPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import logic.Generator;

public class ResetElectronsButtonListener implements ActionListener {
    private Generator generator;
    private BoardPanel boardPanel;
    
    public ResetElectronsButtonListener(Generator generator, BoardPanel boardPanel) {
        this.generator = generator;
        this.boardPanel = boardPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < generator.getBoard().getHeight(); i++)
            for (int j = 0; j < generator.getBoard().getWidth(); j++)
                if(generator.getBoard().getCell(j, i) == CellType.HEAD || generator.getBoard().getCell(j, i) == CellType.TAIL)
                    generator.getBoard().setCell(j, i, CellType.CONDUCTOR);
        
        boardPanel.update(generator);
        boardPanel.repaint();
    }
    
    
}
