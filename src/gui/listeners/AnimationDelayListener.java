package gui.listeners;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AnimationDelayListener implements ChangeListener {
    private StartStopButtonListener startStopButtonListener;
    
    public AnimationDelayListener(StartStopButtonListener listener) {
        this.startStopButtonListener = listener;
    }
    
    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider)e.getSource();
        if (!source.getValueIsAdjusting()) {
            int animationDelay = (int)source.getValue();
            startStopButtonListener.setSleepTime(animationDelay * 100);
        }
        
    }
    
}
