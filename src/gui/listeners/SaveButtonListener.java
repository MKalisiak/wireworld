package gui.listeners;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import logic.Generator;
import logic.IO;

public class SaveButtonListener implements ActionListener {
    private final Generator generator;
    private JFrame frame;
    
    public SaveButtonListener(Generator generator, JFrame frame) {
        this.generator = generator;
        this.frame = frame;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {

        final JFileChooser fileChooser = new JFileChooser();
        int returnVal = fileChooser.showOpenDialog(frame);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            String extension = "";

            int i = file.getName().lastIndexOf('.');
            if (i >= 0) {
                extension = file.getName().substring(i+1);   
            } else {
                String newFileName = file.getPath();
                newFileName = newFileName.concat(".txt");
                File newFile = new File(newFileName);
                file = newFile;
                extension = "txt";
            }
            
            if (extension.equals("txt")) {
                if (file.exists()) {
                    int result = JOptionPane.showConfirmDialog(fileChooser, "The file exists, overwrite?", "Existing file", JOptionPane.YES_NO_CANCEL_OPTION);
                    if (result == JOptionPane.YES_OPTION) {
                        System.out.println("Opening: " + file.getName() + ".");
                        IO.write(generator.getBoard(), file);
                        System.out.println("Saved to file: " + file.getName() + ".");
                    }
                } else {
                    System.out.println("Opening: " + file.getName() + ".");
                    IO.write(generator.getBoard(), file);
                    System.out.println("Saved to file: " + file.getName() + ".");
                }
            } else {
                System.out.println("Cannot create a file other than \"*.txt\".");
            } 
        } else {
            System.out.println("Open command cancelled by user.");
        }
   } 
  
}
