package gui.listeners;

import gui.BoardPanel;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import logic.Board;
import logic.Generator;

public class NewBoardButtonListener implements ActionListener {
    private Generator generator;
    private BoardPanel boardPanel;
    private JFrame frame;
    
    public NewBoardButtonListener(Generator generator, BoardPanel boardPanel, JFrame frame) {
        this.generator = generator;
        this.boardPanel = boardPanel;
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        Window window = SwingUtilities.getWindowAncestor(button);
        
        String heightString = JOptionPane.showInputDialog(window, "Please enter the new board's height:");
        if (heightString == null)
            return;
        int height = 0;
        try {
            height = Integer.parseInt(heightString);
            if (height < 0) {
                JOptionPane.showMessageDialog(window, "Board's height cannot be negative");
            return;
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(window, "Board's height has to be an integer.");
            return;
        }
        
        String widthString = JOptionPane.showInputDialog(window, "Please enter the new board's width:");
        if (widthString == null)
            return;
        int width = 0;
        try {
            width = Integer.parseInt(widthString);
            if (width < 0) {
                JOptionPane.showMessageDialog(window, "Board's width cannot be negative");
            return;
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(window, "Board's width has to be an integer.");
            return;
        }
        
        generator.setBoard(new Board(height, width));
        boardPanel.update(generator);
        boardPanel.repaint();
        frame.pack();
    }
    
}
