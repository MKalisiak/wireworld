package gui.listeners;

import common.CellType;
import gui.BoardPanel;
import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputAdapter;
import elements.Element;
import elements.exceptions.ElementSizeException;

public class BoardPanelListener extends MouseInputAdapter {
    private BoardPanel boardPanel;
    private int previousX;
    private int previousY;
    private boolean drawingWire;
    
    public BoardPanelListener(BoardPanel boardPanel) {
        this.boardPanel = boardPanel;
        this.drawingWire = false;
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        int x = e.getX() / boardPanel.getCellSize();
        int y = e.getY() / boardPanel.getCellSize();
        
        Element element = boardPanel.getSelectedElement();
        
        if(element != null) {
            try {
                element.add(boardPanel.getBoard(), x, y);
            } catch (ElementSizeException ex) {
                System.out.println("Selected element does not fit into the board.");
            }
        } else if (boardPanel.getBoard().isInBounds(x, y)) {
            CellType nextCellType = CellType.EMPTY;
            switch(boardPanel.getCell(x, y)) {
                case EMPTY:
                    drawingWire = true; 
                    nextCellType = CellType.CONDUCTOR; 
                    previousX = e.getX() / boardPanel.getCellSize();
                    previousY = e.getY() / boardPanel.getCellSize();
                    break;
                case CONDUCTOR: 
                    nextCellType = CellType.HEAD;
                    break;
                case HEAD: 
                    nextCellType = CellType.TAIL; 
                    break;
                case TAIL: 
                    nextCellType = CellType.EMPTY; 
                    break;
            }
            boardPanel.setCell(x, y, nextCellType);
        }
        boardPanel.repaint();
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        drawingWire = false;
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        if (drawingWire) {
            int x = e.getX() / boardPanel.getCellSize();
            int y = e.getY() / boardPanel.getCellSize();

            if(boardPanel.getBoard().isInBounds(x, y) && (x != previousX || y != previousY)) {
                boardPanel.setCell(x, y, CellType.CONDUCTOR);
                previousX = x;
                previousY = y;
                boardPanel.repaint();
            }
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        if (boardPanel.getSelectedElement() != null) {
            int x = e.getX() / boardPanel.getCellSize();
            int y = e.getY() / boardPanel.getCellSize();

            previousX = x;
            previousY = y;

            Element element = boardPanel.getSelectedElement();
            boardPanel.setElementX(x);
            boardPanel.setElementY(y);

            boardPanel.repaint();
        }
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        if (boardPanel.getSelectedElement() != null) {
            int x = e.getX() / boardPanel.getCellSize();
            int y = e.getY() / boardPanel.getCellSize();

            if(x != previousX || y != previousY) {
                boardPanel.setElementX(x);
                boardPanel.setElementY(y);
                previousX = x;
                previousY = y;
                boardPanel.repaint();
            }
        }
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        if (boardPanel.getSelectedElement() != null) {
            boardPanel.setElementX(-1);
            boardPanel.setElementY(-1);
            boardPanel.repaint();
        }
    }
    
}
