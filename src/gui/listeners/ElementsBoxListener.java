package gui.listeners;

import gui.BoardPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

public class ElementsBoxListener implements ActionListener {
    private BoardPanel boardPanel;
    
    public ElementsBoxListener(BoardPanel boardPanel) {
        this.boardPanel = boardPanel;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox box = (JComboBox)e.getSource();
        String elementName = (String)box.getSelectedItem();
        boardPanel.setSelectedElement(elementName);
    }
    
}
