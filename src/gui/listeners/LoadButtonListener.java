package gui.listeners;

import gui.BoardPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import logic.Board;
import logic.Generator;
import logic.IO;

public class LoadButtonListener implements ActionListener {
    private Generator generator;
    private BoardPanel boardPanel;
    private JFrame frame;
    
    public LoadButtonListener(Generator generator, BoardPanel boardPanel, JFrame frame) {
        this.generator = generator;
        this.boardPanel = boardPanel;
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final JFileChooser fileChooser = new JFileChooser();
        int returnVal = fileChooser.showOpenDialog(frame);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            
            if (file.exists()) {
                System.out.println("Opening: " + file.getName() + ".");
                try {
                    Board board = IO.read(file);
                    if (board != null) {
                        generator.setBoard(board);
                        boardPanel.update(generator);
                        boardPanel.setSize(boardPanel.getPreferredSize());
                        boardPanel.repaint();
                        frame.pack();
                        System.out.println("Opened file: " + file.getName() + ".");
                    } 
                } catch (IllegalArgumentException ex) {
                    System.out.println("Given file does not have the correct format.");
                }
            } else {
                System.out.println("File: " + file.getName() + " does not exist.");
            }
             
        } else {
            System.out.println("Load command cancelled by user.");
        }
    }
}
