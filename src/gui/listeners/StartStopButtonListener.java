package gui.listeners;

import gui.BoardPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import logic.Generator;

public class StartStopButtonListener implements ActionListener {

    private ActiveThread activeThread;
    private JButton button;
    private Generator generator;
    private BoardPanel boardPanel;
    private int sleepTime;
    
    public StartStopButtonListener(Generator generator, BoardPanel boardPanel, JButton button) {
        this.generator = generator;
        this.boardPanel = boardPanel;
        this.button = button;
        this.activeThread = null;
        this.sleepTime = 100;
    }
    
    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (activeThread == null) {
            button.setText("Stop");
            activeThread = new ActiveThread();
            new Thread(activeThread).start();
            
        } else {
            button.setText("Start");
            activeThread.isRunning = false;
            activeThread = null;
        }
    }
    
    class ActiveThread implements Runnable {

        private boolean isRunning;
        
        private ActiveThread() {
            isRunning = true;
        }
        
        @Override
        public void run() {
            while (isRunning) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    
                }
                generator.nextGeneration();
                boardPanel.repaint();
            }
        }    
    }
    
}
