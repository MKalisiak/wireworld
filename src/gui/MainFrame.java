package gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import logic.Board;
import logic.Generator;
import logic.RulesWireWorld;

public class MainFrame extends JFrame {
    private Generator generator;
    private BoardPanel boardPanel;
    
    public MainFrame(String name) {
        super(name);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
    }
    
    public void displayGUI() {
        Board board = new Board(25, 25);
        generator = new Generator(board, new RulesWireWorld());
        boardPanel = new BoardPanel(generator);

        setLayout(new BorderLayout());
        add(boardPanel, BorderLayout.CENTER);
        add(new MenuPanel(generator, boardPanel, this), BorderLayout.EAST);
        pack();
        setLocationByPlatform(true);
        setVisible(true);
    }
}
