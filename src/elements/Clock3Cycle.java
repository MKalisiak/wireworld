package elements;

import common.CellType;

public class Clock3Cycle extends Element {
   
    public Clock3Cycle() {
        super(2, 3);
        this.layout[0][0] = CellType.EMPTY;
        this.layout[0][1] = CellType.CONDUCTOR;
        this.layout[0][2] = CellType.CONDUCTOR;
        this.layout[1][0] = CellType.CONDUCTOR;
        this.layout[1][1] = CellType.CONDUCTOR;
        this.layout[1][2] = CellType.EMPTY;
    }
    
}
