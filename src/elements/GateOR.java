package elements;

import common.CellType;

public class GateOR extends Element {
    
    public GateOR() {
        super(3, 4);
        this.layout[0][0] = CellType.EMPTY;
        this.layout[0][1] = CellType.CONDUCTOR;
        this.layout[0][2] = CellType.EMPTY;
        this.layout[0][3] = CellType.EMPTY;

        this.layout[1][0] = CellType.CONDUCTOR;
        this.layout[1][1] = CellType.CONDUCTOR;
        this.layout[1][2] = CellType.CONDUCTOR;
        this.layout[1][3] = CellType.CONDUCTOR;

        this.layout[2][0] = CellType.EMPTY;
        this.layout[2][1] = CellType.CONDUCTOR;
        this.layout[2][2] = CellType.EMPTY;
        this.layout[2][3] = CellType.EMPTY;
    }
    
}
