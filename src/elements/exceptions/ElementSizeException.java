package elements.exceptions;

public class ElementSizeException extends RuntimeException{
    
    public ElementSizeException(String message) {
        super(message);
    }
    
}
