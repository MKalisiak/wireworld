package elements;

import common.CellType;

public class GateXOR extends Element {
    
    public GateXOR() {
        super(5, 5);
        this.layout[0][0] = CellType.EMPTY;
        this.layout[0][1] = CellType.CONDUCTOR;
        this.layout[0][2] = CellType.EMPTY;
        this.layout[0][3] = CellType.EMPTY;
        this.layout[0][4] = CellType.EMPTY;

        this.layout[1][0] = CellType.CONDUCTOR;
        this.layout[1][1] = CellType.CONDUCTOR;
        this.layout[1][2] = CellType.CONDUCTOR;
        this.layout[1][3] = CellType.CONDUCTOR;
        this.layout[1][4] = CellType.EMPTY;

        this.layout[2][0] = CellType.CONDUCTOR;
        this.layout[2][1] = CellType.EMPTY;
        this.layout[2][2] = CellType.EMPTY;
        this.layout[2][3] = CellType.CONDUCTOR;
        this.layout[2][4] = CellType.CONDUCTOR;
        
        this.layout[3][0] = CellType.CONDUCTOR;
        this.layout[3][1] = CellType.CONDUCTOR;
        this.layout[3][2] = CellType.CONDUCTOR;
        this.layout[3][3] = CellType.CONDUCTOR;
        this.layout[3][4] = CellType.EMPTY;
        
        this.layout[4][0] = CellType.EMPTY;
        this.layout[4][1] = CellType.CONDUCTOR;
        this.layout[4][2] = CellType.EMPTY;
        this.layout[4][3] = CellType.EMPTY;
        this.layout[4][4] = CellType.EMPTY;
    }
    
}
