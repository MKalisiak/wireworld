package elements;

import common.CellType;

public class DiodeReversed extends Element {
    
    public DiodeReversed() {
        super(3, 6);
        this.layout[0][0] = CellType.EMPTY;
        this.layout[0][1] = CellType.EMPTY;
        this.layout[0][2] = CellType.CONDUCTOR;
        this.layout[0][3] = CellType.CONDUCTOR;
        this.layout[0][4] = CellType.EMPTY;
        this.layout[0][5] = CellType.EMPTY;
        
        for (int i = 0; i < 6; i++)
            this.layout[1][i] = CellType.CONDUCTOR;
        this.layout[1][2] = CellType.EMPTY;
        
        this.layout[2][0] = CellType.EMPTY;
        this.layout[2][1] = CellType.EMPTY;
        this.layout[2][2] = CellType.CONDUCTOR;
        this.layout[2][3] = CellType.CONDUCTOR;
        this.layout[2][4] = CellType.EMPTY;
        this.layout[2][5] = CellType.EMPTY;
    }
    
}
