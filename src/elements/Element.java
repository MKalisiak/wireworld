package elements;

import common.CellType;
import logic.Board;
import elements.exceptions.ElementSizeException;

public abstract class Element {
    protected final int height;
    protected final int width;
    protected final CellType[][] layout;
    
    public Element(int height, int width) {
        this.height = height;
        this.width = width;
        this.layout = new CellType[height][width];
    }

    public void add(Board board, int x, int y) {
        if(board.isInBounds(x, y) && board.isInBounds(x + width - 1, y + height -1)) {
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)  
                    board.setCell(x + j, y + i, layout[i][j]);
        }          
        else
            throw new ElementSizeException("This element does not fit in the board.");
    }
    
    public CellType getCell(int x, int y) {
        CellType cellType = null;
        if (x >= 0 && x < width && y >= 0 && y < height) {
            cellType = layout[y][x];
        } 
        return cellType;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
