package logic;

import common.CellType;

public class Generator {
    private final IRules rules;
    private Board board;
    
    public Generator(Board board, IRules rules) {      
        this.board = board;
        this.rules = rules;
    }

    public void nextGeneration() {
        Board copy = board.copyBoard();
        
        for (int i = 0; i < board.getHeight(); i++)
            for (int j = 0; j < board.getWidth(); j++)
                board.setCell(j, i, rules.nextCellType(copy, j, i));
    }
    
    public Board getBoard() {
        return board;
    }
    
    public void setBoard(Board board) {
        this.board = new Board(board);
    }
}