package logic;

import common.CellType;

public interface IRules {
    
    public CellType nextCellType(Board board, int x, int y);
    
}
