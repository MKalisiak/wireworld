package logic;

import common.CellType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IO {
    
    private static String asText(Board board) {
        String result = "";
        
        result += board.getHeight() + " " + board.getWidth() + "\r\n";
        
        for (int i = 0; i < board.getHeight(); i++) {
            for (int j = 0; j < board.getWidth() - 1; j++)
                result += board.getCell(j, i).toString() + " ";
            result += board.getCell(board.getWidth() - 1, i).toString();
            result += "\r\n";
        }
                
        return result;
    }
    
    public static void write(Board board, String filename) {
        File file = new File(filename);
        
        IO.write(board, file);        
    }
    
    public static void write(Board board, File file) {
        
        try(FileWriter writer = new FileWriter(file)) {
            writer.write(asText(board));
        } catch (IOException e) {
            System.err.println(e);
            System.err.println("Could not write to file: " + file.getName());
        }
    }
    
    public static Board read(String filename) {
        File file = new File(filename);
        
        return IO.read(file);
    }
    
    public static Board read(File file) {
        Board board = null;
        
        try(FileReader fileReader = new FileReader(file)) {
            BufferedReader reader = new BufferedReader(fileReader);
            String line = reader.readLine();
            if (line == null)
                throw new IllegalArgumentException("Wrong input file format.");
                
            String[] dimensions = line.split(" ");
            if (dimensions.length > 2)
                throw new IllegalArgumentException("Wrong input file format.");
            int height = Integer.parseInt(dimensions[0]);
            int width = Integer.parseInt(dimensions[1]);
            board = new Board(height, width);
            
            for (int i = 0; i < height; i++) {
                line = reader.readLine();
                if (line == null)
                    throw new IllegalArgumentException("Wrong input file format.");
                String[] tokens = line.split(" ");
                if (tokens.length != width)
                    throw new IllegalArgumentException("Wrong input file format.");
                for (int j = 0; j < width; j++)
                    board.setCell(j, i, CellType.getCellType(Integer.parseInt(tokens[j])));
            }
            line = reader.readLine();
            if (line != null)
                throw new IllegalArgumentException("Wrong input file format.");
        } catch (IOException e) {
            System.err.println(e);
            System.err.println("Could not read file: " + file.getName());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return null;
        }
        
        return board;
    }
    
}
