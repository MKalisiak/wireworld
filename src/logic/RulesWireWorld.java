package logic;

import common.CellType;

public class RulesWireWorld implements IRules {
    
    @Override
    public CellType nextCellType(Board board, int x, int y) {
        
        int neighbours = numberOfHeadNeighbours(board, x, y);
        CellType result = null;
        
        switch (board.getCell(x, y)) {
            case EMPTY:
                result = CellType.EMPTY;
                break;
            case HEAD:
                result = CellType.TAIL;
                break;
            case TAIL:
                result = CellType.CONDUCTOR;
                break;
            case CONDUCTOR:
                if (neighbours == 1 || neighbours == 2) {
                    result = CellType.HEAD;
                }
                else result = CellType.CONDUCTOR;
                break;
            default:
                result = CellType.CONDUCTOR;
                break;
        }
        
        return result;
    }

    private int numberOfHeadNeighbours(Board board, int x, int y) {
        int counter = 0;
        
        for (int i = -1; i < 2; i++)
            for (int j = -1; j < 2; j++)
                if ((i != 0 || j != 0) && board.isInBounds(x + j, y + i))
                    if (board.getCell(x + j, y + i) == CellType.HEAD)
                        counter++;
        
        return counter;
    }
    
}
