package logic;

import common.CellType;

public class Board {
    private CellType[][] board;
    
    public Board(CellType[][] board) {
        if (board != null) {
            this.board = new CellType[board.length][board[0].length];
            
            for (int i = 0; i < board.length; i++)
                System.arraycopy(board[i], 0, this.board[i], 0, board[0].length); 
        }
        else 
            this.board = null;
    }
    
    public Board(int height, int width) {
        this.board = new CellType[height][width];
        
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                this.board[i][j] = CellType.EMPTY;
    }
    
    public Board(int [][] intBoard) {
        if (intBoard != null) {
            this.board = new CellType[intBoard.length][intBoard[0].length];
            
            for (int i = 0; i < intBoard.length; i++)
                for (int j = 0; j < intBoard[0].length; j++)
                    this.board[i][j] = CellType.getCellType(intBoard[i][j]);
        }
        else 
            this.board = null;
    }

    public Board(Board board) {
        if (board != null) {
            this.board = new CellType[board.getHeight()][board.getWidth()];
            for (int i = 0; i < board.getHeight(); i++)
                for (int j = 0; j < board.getWidth(); j++)
                    this.board[i][j] = board.getCell(j, i);                
        }
        else
            this.board = null;
    }
    
    public int getHeight() {
        return board.length;
    }

    public int getWidth() {
        if (getHeight() > 0)
            return board[0].length;
        else 
            return 0;
    }
    
    public CellType getCell(int x, int y) {
        return board[y][x];
    }
    
    public void setCell(int x, int y, CellType cellType) {
            this.board[y][x] = cellType;
        }
    
    public boolean isInBounds(int x, int y) {
        return x >= 0 && x < getWidth() && y >= 0 && y < getHeight();
    }
    
    public Board copyBoard() {
        Board result = new Board(board);       
        return result;
    }
    
    
}
