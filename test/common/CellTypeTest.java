package common;

import org.junit.Test;
import static org.junit.Assert.*;

public class CellTypeTest {
    
    public CellTypeTest() {
    }

    @Test
    public void testToString() {
        CellType cellType = CellType.HEAD;
        assertEquals("1", cellType.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCellType() {
        assertEquals(CellType.CONDUCTOR, CellType.getCellType(3));
        assertEquals(CellType.HEAD, CellType.getCellType(1));
        assertEquals(CellType.TAIL, CellType.getCellType(2));
        assertEquals(CellType.EMPTY, CellType.getCellType(0));
        CellType.getCellType(-1);
    }
    
}
