package logic;

import java.io.File;
import org.junit.Test;
import static org.junit.Assert.*;

public class IOTest {
    
    public IOTest() {
    }

    @Test
    public void testWrite() {
        int[][] intBoard = {
            {0, 3, 3, 3},
            {2, 1, 3, 0},
            {3, 3, 3, 0}
        };
        Board board = new Board(intBoard);
        IO.write(board, "test-files/testwrite.txt");
        File file = new File("test-files/testwrite.txt");
        assertEquals(true, file.exists());
    }

    @Test
    public void testRead() {
        int[][] intBoard = {
            {0, 3, 3, 3},
            {3, 1, 2, 0},
            {3, 3, 3, 0}
        };
        Board board = new Board(intBoard);
        IO.write(board, "test-files/testread.txt");
        Board result = IO.read("test-files/testread.txt");
        for (int i = 0; i < board.getHeight(); i++)
            for (int j = 0; j < board.getWidth(); j++)
                assertEquals(board.getCell(j, i), result.getCell(j, i));
    }
    
    @Test
    public void testReadWithWrongInput() {
        Board board = IO.read("test-files/wrongdata.txt");
        assertEquals(null, board);
    }
    
}
