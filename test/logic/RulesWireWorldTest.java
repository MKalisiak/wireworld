package logic;

import common.CellType;
import org.junit.Test;
import static org.junit.Assert.*;

public class RulesWireWorldTest {
    
    public RulesWireWorldTest() {
    }

    @Test
    public void testNextCellType() {
        int[][] intBoard = {
            {0, 3, 3, 3},
            {2, 1, 3, 0},
            {3, 3, 3, 0}
        };
        Board board = new Board(intBoard);
        IRules rules = new RulesWireWorld();
        assertEquals(CellType.EMPTY, rules.nextCellType(board, 0, 0));
        assertEquals(CellType.CONDUCTOR, rules.nextCellType(board, 0, 1));
        assertEquals(CellType.TAIL, rules.nextCellType(board, 1, 1));
        assertEquals(CellType.HEAD, rules.nextCellType(board, 1, 0));
        assertEquals(CellType.HEAD, rules.nextCellType(board, 2, 0));
        assertEquals(CellType.HEAD, rules.nextCellType(board, 2, 1));
        assertEquals(CellType.HEAD, rules.nextCellType(board, 2, 2));
        assertEquals(CellType.HEAD, rules.nextCellType(board, 1, 2));
        assertEquals(CellType.HEAD, rules.nextCellType(board, 0, 2));
    }
    
}
