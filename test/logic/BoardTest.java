package logic;

import common.CellType;
import org.junit.Test;
import static org.junit.Assert.*;

public class BoardTest {
    
    public BoardTest() {
    }

    @Test
    public void testGetHeight() {
        Board board = new Board(15, 20);
        assertEquals(15, board.getHeight());
    }

    @Test
    public void testGetWidth() {
        Board board = new Board(15, 20);
        assertEquals(20, board.getWidth());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetCell() {
        int[][] intBoard = {
            {0, 3, 3, 3},
            {2, 1, 3, 0},
            {3, 3, 3, 0}
        };
        Board board = new Board(intBoard);
        assertEquals(CellType.EMPTY, board.getCell(0, 0));
        assertEquals(CellType.CONDUCTOR, board.getCell(3, 0));
        assertEquals(CellType.HEAD, board.getCell(1, 1));
        assertEquals(CellType.TAIL, board.getCell(0, 1));
        board.getCell(-1, -1);
    }

    @Test
    public void testSetCell() {
        Board board = new Board(5, 5);
        assertEquals(CellType.EMPTY, board.getCell(2, 2));
        board.setCell(2, 2, CellType.HEAD);
        assertEquals(CellType.HEAD, board.getCell(2, 2));
    }

    @Test
    public void testIsInBounds() {
        Board board = new Board(5, 5);
        assertEquals(true, board.isInBounds(0, 0));
        assertEquals(true, board.isInBounds(4, 4));
        assertEquals(false, board.isInBounds(4, 5));
        assertEquals(false, board.isInBounds(1, -1));
    }

    @Test
    public void testCopyBoard() {
        int[][] intBoard = {
            {0, 3, 3, 3},
            {2, 1, 3, 0},
            {3, 3, 3, 0}
        };
        Board board = new Board(intBoard);
        Board copy = board.copyBoard();
        for (int i = 0; i < board.getHeight(); i++)
            for (int j = 0; j < board.getWidth(); j++)
                assertEquals(board.getCell(j, i), copy.getCell(j, i));
    }
    
}
