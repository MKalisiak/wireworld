package logic;

import common.CellType;
import org.junit.Test;
import static org.junit.Assert.*;

public class GeneratorTest {
    
    public GeneratorTest() {
    }

    @Test
    public void testNextGeneration() {
        int[][] intBoard = {
            {0, 3, 3, 3},
            {2, 1, 3, 0},
            {3, 3, 3, 0}
        };
        CellType[][] nextBoard = {
            {CellType.EMPTY, CellType.HEAD, CellType.HEAD, CellType.CONDUCTOR},
            {CellType.CONDUCTOR, CellType.TAIL, CellType.HEAD, CellType.EMPTY},
            {CellType.HEAD, CellType.HEAD, CellType.HEAD, CellType.EMPTY}
        };
        Board board = new Board(intBoard);
        Generator generator = new Generator(board, new RulesWireWorld());
        generator.nextGeneration();
        Board result = generator.getBoard();
        for (int i = 0; i < board.getHeight(); i++)
            for (int j = 0; j < board.getWidth(); j++)
                assertEquals(nextBoard[i][j], result.getCell(j, i));
    }

    @Test
    public void testGetBoard() {
        CellType[][] cellBoard = {
            {CellType.EMPTY, CellType.CONDUCTOR, CellType.CONDUCTOR, CellType.CONDUCTOR},
            {CellType.TAIL, CellType.HEAD, CellType.CONDUCTOR, CellType.EMPTY},
            {CellType.CONDUCTOR, CellType.CONDUCTOR, CellType.CONDUCTOR, CellType.EMPTY}
        };
        Board board = new Board(cellBoard);
        Generator generator = new Generator(board, new RulesWireWorld());
        Board result = generator.getBoard();
        for (int i = 0; i < board.getHeight(); i++)
            for (int j = 0; j < board.getWidth(); j++)
                assertEquals(cellBoard[i][j], result.getCell(j, i));
    }
    
}
